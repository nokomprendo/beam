with import <nixpkgs> {}; 

stdenv.mkDerivation {
    name = "beam";
    src = ./.;
    buildInputs = [ texlive.combined.scheme-full ];

    buildPhase = "latexmk -xelatex -shell-escape main";

    installPhase = ''
        mkdir -p $out/public
        cp main.pdf $out/public/
    '';
}

